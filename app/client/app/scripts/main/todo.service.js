'use strict';

angular.module('todo')
.factory('Todo', function ($resource) {
    return $resource('http://ubuntu.xepe:8000/todos/:id', {id: '@id'}, {
        'update': { method: 'PUT' }
    });
});
