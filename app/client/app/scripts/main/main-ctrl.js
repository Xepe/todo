'use strict';

angular.module('todo')
.controller('MainCtrl', function ($scope, todos, Todo) {
    $scope.todo = {};
    $scope.todos = todos; 

    $scope.addTodo= function () {
        $scope.isAdding = true;
    };

    $scope.createTodo = function (todo) {
        $scope.isAdding = false;
        Todo.save(todo, function (data) {
            $scope.todos.push(new Todo(data));
        });
    };

    $scope.cancelTodo = function () {
        $scope.isAdding = false;
    };

    $scope.completeTodo = function (todo) {
        todo.done = true;
        todo.$update();
    };

    $scope.deleteTodo = function (todo, index) {
        todo.$delete(function () {
            $scope.todos.splice(index, 1);
        });
    };
});
