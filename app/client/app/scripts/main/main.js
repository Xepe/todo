'use strict';

angular.module('todo')
.config (function ($stateProvider) {
    $stateProvider
    .state('main',{
        url: '/',
        templateUrl: 'partials/main.html',
        controller: 'MainCtrl',
        resolve: {
            todos: function (Todo) {
                return Todo.query();
            }
        }
    });
});


