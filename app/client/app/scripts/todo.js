'use strict';

angular.module('todo', [
    'ui.router',
    'ngResource'
]).config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider
    .otherwise('/');
});
