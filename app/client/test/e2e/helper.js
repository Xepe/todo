global.chai = require('chai');
global.chaiAsPromised = require('chai-as-promised');
global.expect = chai.expect;
global.chai.use(chaiAsPromised);
