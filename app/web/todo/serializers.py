from rest_framework import serializers

from todo.models import Todo
from django.contrib.auth.models import User


class TodoSerializer(serializers.ModelSerializer):
    #owner = serializers.Field(source='owner.username')

    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'done')


class UserSerializer(serializers.ModelSerializer):
    todos = serializers.PrimaryKeyRelatedField(many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'todos')
