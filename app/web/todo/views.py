"""
here we go
"""
from rest_framework import generics

from todo.models import Todo
from django.contrib.auth.models import User

from todo.serializers import TodoSerializer
from todo.serializers import UserSerializer

from rest_framework import permissions
from todo.permissions import IsOwnerOrReadOnly


class TodoList(generics.ListCreateAPIView):
    """
    List all code todos, or create a new todo.
    """
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          #IsOwnerOrReadOnly)

    def pre_save(self, obj):
        obj.owner = self.request.user


class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    List all code todos, or create a new todo.
    """
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          #IsOwnerOrReadOnly)


class UserList(generics.ListCreateAPIView):
    """
    List all code todos, or create a new todo.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
