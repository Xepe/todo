from django.db import models


class Todo (models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    description = models.CharField(max_length=100, blank=True, default='')
    done = models.BooleanField(blank=True, default=False)

    class Meta:
        ordering = ('created',)
