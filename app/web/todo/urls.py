from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from todo import views


urlpatterns = patterns('todo.views', 
                       url(r'^todos$', views.TodoList.as_view()),
                       url(r'^todos/(?P<pk>[0-9]+)$', 
                           views.TodoDetail.as_view()),
                       url(r'^users/$', views.UserList.as_view()))

urlpatterns = format_suffix_patterns(urlpatterns)
