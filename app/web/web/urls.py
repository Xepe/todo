from django.conf.urls import patterns, include, url
from django.contrib import admin
from .views import Home


urlpatterns = patterns('', url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', Home.as_view(), name='home'),
                       url(r'^', include('todo.urls')),
                       url(r'^api-auth/', include('rest_framework.urls',
                                                  namespace='rest_framework')))
